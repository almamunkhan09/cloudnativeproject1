FROM python:alpine3.16

WORKDIR /app
COPY . /app

RUN pip install -r requirements.txt
RUN python init_db.py

ENTRYPOINT [ "python" ]
CMD [ "app.py" ]


